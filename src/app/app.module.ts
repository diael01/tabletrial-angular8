import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TABLEComponent } from './table/table.component';
import { DataTablesModule } from 'angular-datatables';
//import { DataTablesModule } from 'datatables.net-dt';
import { HttpModule } from '@angular/http';
import { Table1Component } from './table1/table1.component';

//import { NgGUDCoreModule } from 'ng-vfor-lib';
import { VirtualScrollerModule, VirtualScrollerDefaultOptions } from 'ngx-virtual-scroller';

@NgModule({
  declarations: [
    AppComponent,
    TABLEComponent,
    Table1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DataTablesModule,
    HttpModule,
    //NgGUDCoreModule,
   VirtualScrollerModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, ViewChild, ViewContainerRef, TemplateRef } from '@angular/core';
import { Http , Response } from '@angular/http';
import { Data } from '../../assets/data';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-table1',
  templateUrl: './table1.component.html',
  styleUrls: ['./table1.component.sass']
})

export class Table1Component implements OnInit {
  allData: Data[] = [];
  obs: Subject<Data[]> = new Subject<Data[]>();

  @ViewChild('itemsContainer', { read: ViewContainerRef }) container: ViewContainerRef;
  @ViewChild('item', { read: TemplateRef }) template: TemplateRef<any>;

  constructor(private http: Http) { }

  ngOnInit(): void {

    this.http.get('assets/sample_data_s.json')
    .pipe(map(this.getData))
    .subscribe(allData => 
      {
        this.allData = allData;
        this.obs.next();
    });
  }
   
  private getData(res: Response) {
    return res.json();
  } 

  private renderData(length: number) {
    const all = 1000;
    const time = 100;
  
    let currentIndex = 0;
  
    const interval = setInterval(() => {
      const nextIndex = currentIndex + all;
  
      for (let n = currentIndex; n <= nextIndex ; n++) {
        if (n >= length) {
          clearInterval(interval);
          break;
        }
        const context = {
          item: {
            id: n,
            name: Math.random()
          },
          isEven: n % 2 === 0
        };
        this.container.createEmbeddedView(this.template, context);
      }
  
      currentIndex += all;
    }, time);
  }

}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Http , Response } from '@angular/http';
import { Subject } from 'rxjs';
import { Data } from '../../assets/data';
import { map } from 'rxjs/operators';

//Comment: use PouchDB for big starage data in UI
//import PouchDB from 'pouchdb';

//Comment: use JQUERY for sticky header, footer
//declare var $: any;
import { DataTables } from 'datatables.net-dt';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TABLEComponent implements OnDestroy, OnInit {
  dtOptions: DataTables.Settings = {};
  allData: Data[] = [];
  db: any = {};
  //Comment: used for import JSON file via import not via http
  //allData: Data[] = (data as any).default; 
  dtTrigger: Subject<Data[]> = new Subject<Data[]>();

  constructor(private http: Http) { }

        ngOnInit(): void {
          this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 3
          }; 

   
        this.http.get('assets/sample_data.json')
        .pipe(map(this.readDataOneTime))
        .subscribe(allData => 
          {
            this.allData = allData;
            //Comment: PouchDB used for caching data
            /* if(this.db == null} 
              { 
                  this.db = new PouchDB('bigstorage');
                for(var i = 0; i< this.allData.length; i++)
                {
                  this.add(allData[i]);
                  console.log("data added");
                } 
              }*/
            this.dtTrigger.next();
        });
        
        /* else {  //fill up from DB
          this.allData = this.getAll();
        } */
      }

      readDataOneTime(res: Response) {
        return res.json();
      } 

      modify(id: string): void {
        console.log(id);
      }
  
      //Comment: TBU for caching
      add(item: Data) {
        const response = this.db.put(item);
      }

      //Comment: TBU for caching
      getAll(){
        return this.db.get();
      }
      
      //Comment: TBU for caching
      get(item: Data){
        return this.db.get(item);
      }


      ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
      }


    //Comment : all trials for using JQUERY for sticky HEADER/FOOTER
    /*$(document).ready(function() {
      
      
      $('#DataTables_Table_0').DataTable({
        "fixedHeader": {
          header: true,
        },
        "fixedFooter": {
          footer: true,
        },
        "bLengthChange": false,
        "Filter": false,
        "Info": false,
        'pagingType': 'full_numbers',
        'pageLength': 100
      });
    

    });

    
   
    $(document).ready(function() {
      $('.device-table').DataTable({
        "fixedHeader": {
          header: true,
        },
        "bLengthChange": false,
        "Filter": false,
        "Info": false,
      });
    
    });
    

    $('#t1').DataTable( {
      fixedHeader: true
  } );

    this.dtOptions = DTOptionsBuilder
    .newOptions()
    .withOption('paging', false)
    .withDataProp('results')
    .withFixedHeader({
        bottom: true
    })
; */
  
      
}

// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { TABLEComponent } from './table.component';
import { Http } from '@angular/http';
import { 
  BrowserDynamicTestingModule, 
  platformBrowserDynamicTesting 
} from '@angular/platform-browser-dynamic/testing';


@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({name: 'translate'})
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({name: 'phoneNumber'})
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({name: 'safeHtml'})
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('TABLEComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, ReactiveFormsModule ],
      declarations: [
        TABLEComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
      providers: [
        Http
      ]
    }).overrideComponent(TABLEComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(TABLEComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function() {};
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    component.http = component.http || {};
    component.http.get = jest.fn().mockReturnValue(observableOf({}));
    component.dtTrigger = component.dtTrigger || {};
    component.dtTrigger.next = jest.fn();
    component.ngOnInit();
    expect(component.http.get).toHaveBeenCalled();
    expect(component.dtTrigger.next).toHaveBeenCalled();
  });

  it('should run #add()', async () => {
    component.db = component.db || {};
    component.db.put = jest.fn().mockReturnValue(observableOf('put'));
    component.add({});
    expect(component.db.put).toHaveBeenCalled();
  });

  it('should run #getAll()', async () => {
    component.db = component.db || {};
    component.db.get = jest.fn();
    component.getAll();
    expect(component.db.get).toHaveBeenCalled();
  });

  it('should run #get()', async () => {
    component.db = component.db || {};
    component.db.get = jest.fn();
    component.get({});
    expect(component.db.get).toHaveBeenCalled();
  });

  it('should run #ngOnDestroy()', async () => {
    component.dtTrigger = component.dtTrigger || {};
    component.dtTrigger.unsubscribe = jest.fn();
    component.ngOnDestroy();
    expect(component.dtTrigger.unsubscribe).toHaveBeenCalled();
  });

  it('should run #readDataOneTime()', async () => {

    component.readDataOneTime({
      json: function() {}
    });

  });

  it('should run #modify()', async () => {

    component.modify({});

  });

});
